_base_ = './db_cascade_mask_rcnn_r50_fpn_1x_coco.py'
model = dict(
    pretrained='/home/ec2-user/cdecnet_khoi/CDeCNet/model/db_cascade_mask_rcnn_x101_32x4d_fpn_dconv_c3-c5_1x_coco-e75f90c8.pth',
    backbone=dict(
        type='DB_ResNeXt',
        depth=101,
        groups=32,
        base_width=4,
        num_stages=4,
        out_indices=(0, 1, 2, 3),
        frozen_stages=2,
        norm_cfg=dict(type='BN', requires_grad=True),
        style='pytorch'))
